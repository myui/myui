# Mozi.MyUI简介
Mozi.MyUI是一套精巧的前端框架,目标是解决移动WEB开发中的快速响应和渲染需求.整个框架仅仅包含必要的功能.

## 功能
框架包含:
1. Javascript库
2. CSS样式库
3. 图标库

## 三方库引用说明
- [Font Awesome][fontawesome] SIL OFL协议

## 项目地址
- [Gitee][gitee]

## 功能与版本迭代
	不定期对MyUI的功能进行完善,解决各种BUG。

## 版权说明
	本项目采用MIT开源协议，引用请注明出处。欢迎复制，引用和修改。意见建议疑问请联系软件作者，或提交ISSUE。
	
## 用例说明
~~~javascript
    //JS加载完毕后触发
     m.onReady(function(){
        //示例代码
        m.ajax.request({
            url:"package.json",
            params:{
                rc:"123456789",
                v:"1.1.1"
            },
            success:function (resp,text) {
                var data=m.JSON.decode(text);
                var oTitle=m.get("title"),oDesc=m.get("description");
                oTitle.innerText=data["name"];
                oDesc.innerText="版本"+data["version"];
            }
        });

     });
~~~

### By [Jason][1] on Mar. 6,2015

[1]:mailto:brotherqian@163.com
[gitee]:https://gitee.com/myui/myui.git
[fontawesome]:https://github.com/FortAwesome/Font-Awesome