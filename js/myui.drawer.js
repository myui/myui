/**
 * @module  draw plugin
 */
m.drawer=(new function(){
    var me=this;
    me.Id="";
    me.penColor="#FF0000";me.penWidth=0;
    me.posX=0;me.posY=0;
    me.isDown=false;me.isMove=false;
    me.Track=[];me.Pos={x:0,y:0};
    me.Events={
        onTouchStart:function(e){
            e.preventDefault();
            var self= m.drawer,canvas=self.Canvas,oTouch= e.touches[0];
            canvas.style.borderColor="#808080";
            self.isDown=true;
            self.isMove=true;
            //计算触摸位置
            self.posX=oTouch.pageX;
            self.posY=oTouch.pageY-(window.scrollY||window.pageYOffset);
        },
        onTouchMove:function(e){
            e.preventDefault();
            var self = m.drawer, canvas = self.canvas, ctx = self.Context, oTouch = e.touches[0];
            if(e.touches.length>1){m.drawer.redo();}
            if (self.isDown) {
                if (self.isMove) {
                    ctx.moveTo(self.posX, self.posY);
                    self.isMove = false;
                }
                self.posX = oTouch.pageX;
                self.posY = oTouch.pageY-(window.scrollY||window.pageYOffset);
                self.Pos = {"x": self.posX, "y": self.posY};
                self.Track.push(self.Pos);
                ctx.lineTo(self.posX, self.posY);
                ctx.stroke();
            }
        },
        onTouchEnd:function(e){
            e.preventDefault();
            var self= m.drawer;
            self.isDown=false;
            self.isMove=false;
        }
    };
    me.render/*渲染绘图对象*/=function(id,option){
        var self= m.drawer,p=option,elDock=m.dom.get(id),elCanvas=m.dom.get('DrawBoard')||m.dom.create('canvas');
        self.Track.splice(0,self.Track.length);
        self.Id=id;
        self.penColor=p.penColor||self.penColor;
        self.penWidth= p.penWidth;
        //初始化Canvas元素
        elCanvas.id="DrawBoard";
        with(elCanvas){
            width= p.width;
            height= p.height;
            id='DrawBoard';
            style.margin='auto';
        }
        elDock.appendChild(elCanvas);
        var ctx=elCanvas.getContext('2d');
        //初始化canvas上下文对象
        with(ctx){
            lineWidth/*线宽*/=self.penWidth;
            strokeStyle/*描绘颜色*/=self.penColor;
            fillStyle/*填充颜色*/=self.penColor;
            lineCap/*线头样式*/="round";
            lineJoin/*转角样式*/="round";
            miterLimit/*折角锐利度*/=1;
            shadowColor='#FC0000';
            shadowBlur=1;
        }
        //为Canvas元素附加事件
        m.addListener(elCanvas,"touchstart",self.Events.onTouchStart);
        m.addListener(elCanvas,"touchmove",self.Events.onTouchMove);
        m.addListener(elCanvas,"touchend",self.Events.onTouchEnd);
        //赋值对象
        self.Canvas=elCanvas;
        self.Context=ctx;
    };
    me.redo/*清空所有*/=function(){
        var self= m.drawer,canvas=self.Canvas,ctx=self.Context;
        ctx.clearRect(0,0,canvas.width,canvas.height);
        canvas.width=canvas.width;
        //置空路径数组
        self.Track.splice(0,self.Track.length);
        self.render(self.Id,{width:canvas.width,height:canvas.height});
    };
    //画入ImageData
    me.drawImage=function(data){
        var self= m.drawer,ctx=self.Context;
        ctx.drawImage(data,0,0);
    };
    me.formatText/*绘制水印*/=function(text){
        var self= m.drawer,ctx=self.Context;
        ctx.font="2.25rem 宋体";
        ctx.fillText(text,250,300);
        ctx.stroke();
    };
    //取png数据类型
    me.getImage=function(){
        var self= m.drawer;
        console.log(self.Canvas.toDataURL('image/png'));
        return self.Canvas.toDataURL('image/png');
    };
}());
