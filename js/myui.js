/**
 * @class MyUI Created on 2015-03-06
 * @author Jason @see brotherqian@163.com
 */
var m=m||{};
m.version="1.0";
m.Ajax=(new function(){
    var me=this;
    me.request=function(p,oBind){
        var a=new XMLHttpRequest(),url=p.url,ep=p.params,arrQr=[],nullText=p.nullText,method= p.method||"GET";
        a.timeout=  p.timeout||30000;
        var data;
        for (var r in ep) {arrQr.push(r + '=' + ep[r]);}
        data=arrQr.join("&");
        if(method=="GET"){url=url+"?"+data;}else{url=url+"?rc="+ m.util.crypt.getParity(8)}
        a.open(method, url, p.async||true);
        a.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
        var oMask,oLoad,iDeg=0;
        if(oBind){
            oBind.add=oBind.appendChild;
            oMask= m.create("div",{cls:"x-mask-default"});
            oLoad=m.create("span",{html:"&nbsp;加载中..."});
            oMask.add(oLoad);
            oBind.add(oMask);
        }
        a.onreadystatechange=function(){
            me.state= a.readyState;
            if(me.state===4){
                var cb= p.callback;
                setTimeout(function(){m.dom.remove(oMask);iDeg=null;},100);
                if(p.success&& a.status===200) {p.success(a, a.responseText)
                }else if(p.failure) {p.failure(a);}
                if(nullText&&(!Boolean(a.responseText)||a.responseText)=='[]'&&oBind){
                    var elEmpty= m.create('div',{cls:"x-panel-empty", html:nullText});
                    oBind.add(elEmpty);
                }
                if(cb){cb(a);}
            }else{if(Boolean(oMask)){oMask.style.display="block";}}
        };
        a.send(method==="POST"? data:null);
    };
}());
m.anim=(new function(){
    var me=this;
    me.float=function(el,floatEL,fullscreen,pos){
        m.addListener(el,"focus",function(){
            me.mask(floatEL,fullscreen,pos);
        });
        m.addListener(el,"blur",function(){
            me.unmask(floatEL);
        });
    };
    me.mask=function(el,fullscreen,pos){
        el.style.position="fixed";
        switch (pos){
            case "top":
                el.style.left="0";
                el.style.top="0";
                break;
            case "bottom":
            default :
                el.style.left="0";
                el.style.bottom="0";
                break;
        }
        if(fullscreen){
            el.style.bottom="0";
            el.style.right= "0";
            el.style.top="0";
        }
    };
    me.unmask=function(el){
       el.style.position="static";
       delete el.style.width;
       delete el.style.height;
    };
    me.transform=function(el,trans,origin){
        var s=el.style;
        s.transform=trans;
        s.mozTransform=trans;
        s.webkitTransform=trans;
        if(!Boolean(origin)){
           origin="50% 50%";
        }
        s.transformOrigin=origin;
        s.webkitBackgroundOrigin=origin;
        s.mozTransformOrigin=origin;
    }
}());
m.dom = new function () {
    var me = this, doc = document;
    me.head = doc.head;me.body = doc.body;me.doc = doc;m.title = doc.title;
    me.body.addChild= me.body.appendChild;me.body.add=me.body.addChild;
    me.addChild=function(child,el){
        return el.appendChild(child);
    };
    me.get = function (s, el) {
        var regId = /^\w+/gi, d = el || doc;
        if (regId.test(s)) {
            var r= d.getElementById(s);
            if(r){r.addChild= r.appendChild;r.add= r.appendChild;}
            return r;
        }
    };
    me.query = function (pm, el) {
        var d = el || doc;
        return d.querySelector(pm);
    };
    me.queryAll = function (pm, el) {
        var d = el || doc;
        return d.querySelectorAll(pm);
    };
    me.remove = function (s) {
        var sType = typeof s;
        switch (sType) {
            case 'string':{var d = m.dom.get(s);if (Boolean(d)) {d.parentNode.removeChild(d);}}break;
            case 'object':s.parentNode.removeChild(s);break;
            default:return;
        }
    };
    me.getValue = function (d) {
        var elDocker = d.lastChild, elInput = elDocker.firstChild, sResult = null;
        switch (elInput.type) {
            case "text":
            case "password":
                sResult = elInput.value;
                break;
            case "checkbox":
                sResult = elInput.checked;
                break;
            default:
                break;
        }
        return sResult;
    };
    me.setValue = function (d, value) {
        var elDock = d.lastChild, elCell = elDock.lastChild, oR = value;
        switch (elCell.type) {
            case "text":
            case "password":elCell.value = oR;break;
            case "checkbox":elCell.checked = Boolean(oR);break;
            default:break;
        }
        return oR;
    };
    me.create = function (d,pm) {
        var el=m.Doc.createElement(d);
        el.addChild=el.appendChild;
        el.add=el.appendChild;
        if(pm){
            pm["className"] = pm['cls'] || pm["className"]||null;
            pm["innerHTML"] = pm["html"] || pm["innerHTML"]||null;
            for(var r in pm){
                if((typeof pm[r])=='string'){
                    el[r]=pm[r];
                }else{
                    for(var f in pm[r]){
                        el[r][f]=pm[r][f];
                    }
                }
            }
        }
        return el;
    };
}();
m.EventUtil={
    posX:0, posY:0, target:null,
    handlers:{
        onTouchStart:function(){},
        onTouchEnd:function(){},
        onSwipeLeft:function(){},
        onSwipeRight:function(){},
        onSwipeUp:function(){},
        onSwipeDown:function(){},
        onScroll:function(){},
        onScrollUp:function(){},
        onScrollDown:function(){},
        onOrientationChange:function(){}
    },
    addListener:function(el,type,fn,capture){
        if(el.addEventListener){el.addEventListener(type,fn,capture||false);}
        else if(el.attachEvent){el.attachEvent("on"+type,fn);}
        else{el["on"+type]=fn;}
        this.target=el;
    },
    removeListener:function(el,type,handler){
        if(el.removeEventListener){el.removeEventListener(type,handler,false);}
        else if(el.detachEvent){el.detachEvent(type,handler,false);}
        else{el["on"+type]=null;}
    }
};
m.EmptyImage="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
m.JSON=(new (function(){
    var me=this;
    me.encode=function(o){try{ return JSON.stringify(o)}catch(e) {return "";}};
    me.decode=function(o){try{return JSON.parse(o)}catch (e){return null;}};
})());
m.String=(new function(){
    var me=this;
    me.format=function(tpl,values){
        var args=Array.prototype.slice.call(arguments);
        args.shift();
        var reg=/{\w+}/gi;
        var fn=function(m,name,format){
            var f= m.substr(1, m.length-2);
            return args[parseInt(f)]!==null?args[parseInt(f)]:"";
        };
        return tpl.replace(reg,fn);
    }
});
m.Temp=(new (function(){
    var me=this;
    me.apply=function(tmp,data){
        var regField=/{\w+}/gi,tplHtml=tmp.innerHTML;
        var fn=function(m,name,format,args){
            var f= m.substr(1, m.length-2);
            return data[f]!==null?data[f]:"";
        };
        return tplHtml.replace(regField, fn);
    }
})());
m.util=(new (function(){
    var me=this;
    //加密单元
    me.crypt=(new (function(){
        var me=this,sead=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9','+','/','='];
        me.getParity=function(l){
           var data="";
           for(var i=0;i<l;i++) {
               var iR=Math.ceil(Math.random()*61);
               iR=i%2==0?(iR=iR%2==0?iR:iR-1):(iR=iR%2==1?iR:iR-1);
               data+=sead[iR];
           }
           return data;
        };
        me.getBase64=function(key){
            var out = '', hex = 0, i = 0, len = key.length,buf = [],sRes =sead.join("");
            var dg1, dg2, dg3, dg4;
            while (i < len) {
                var bLen=buf.length;
                hex = key.charCodeAt(i++);
                if (hex < 0x80) {buf[bLen] = hex;}
                else if (hex < 0x800) {buf[bLen] = 0xc0 | (hex >> 6);buf[bLen] = 0x80 | (hex & 0x3f);}
                else if (hex < 0x10000) {buf[bLen] = 0xe0 | (hex >> 12);buf[bLen] = 0x80 | ((hex >> 6) & 0x3f);buf[bLen] = 0x80 | (hex & 0x3f);}
                else {buf[bLen] = 0xf0 | (hex >> 18);buf[bLen] = 0x80 | ((hex >> 12) & 0x3f);buf[bLen] = 0x80 | ((hex >> 6) & 0x3f);buf[bLen] = 0x80 | (hex & 0x3f);}
                if (i == len) {while (bLen % 3) {buf[bLen] = NaN;}}
                if (bLen > 2) {
                    dg1 = buf[0] >> 2;dg2 = ((buf[0] & 3) << 4) | (buf[1] >> 4);dg3 = ((buf[1] & 15) << 2) | (buf[2] >> 6);dg4 = buf[2] & 63;
                    if (isNaN(buf[1])) {dg3 = dg4 = 64;} else if (isNaN(buf[2])) {dg4 = 64;}
                    buf.splice(0,3);out=out+sRes.charAt(dg1)+sRes.charAt(dg2)+sRes.charAt(dg3)+sRes.charAt(dg4);
                }
            }
            return out;
        }
    })());
    me.formatSize=function(size){
        return size<1024?size+"B":size<1048576?parseFloat(size/1024).toFixed(2)+"KB":size<1073741824?parseFloat(size/1048576).toFixed(2)+"MB":">1GB";
    }
})());
m.window=(new (function(){
    var me=this,sTitle="消息",sDlg=null,fn,sMsg;
    me.dock={};me.mask={};me.win={};
    var createWinMsg=function(){
        var mask= m.create("div",{cls:"x-win-mask"}), win= m.create('div',{cls:"x-win-msg"}), bd= m.create('div',{cls:"x-win-body",html:sMsg}),
            title= m.create('div',{cls:"x-win-title",html:sTitle}), tl= m.create('div',{cls:"x-win-tool"});
        var btnYes= m.create("div",{cls:"x-tool-item",html:"确&nbsp;认"}), btnNo= m.create('div',{cls:"x-tool-item",html:"关&nbsp;闭"}), btnClose= m.create('div');
        tl.addChild(btnYes);tl.addChild(btnNo);win.addChild(title);win.addChild(bd);win.addChild(tl);
        m.addListener(win,"click",function(e){
           var tg= e.target;
           switch (tg){case  btnYes:sDlg='yes';break;case btnNo:sDlg='no';break;case btnClose:default :sDlg=null;break;}
           if(sDlg){
               m.remove(mask);
               m.remove(win);
               if(fn){fn(sDlg);}
           }
        });
        me.mask=mask;
        me.win=win;
        return win;
    };
    var createWinWait=function(){
        var mask= m.create("div",{cls:"x-win-mask"}),win= m.create("div",{cls:"x-win-wait"});
        me.mask=mask;me.win=win;
        win.close=function(){
            if(win) {
                m.remove(mask);
                m.remove(win);
            }
        };
        return win;
    };
    var renderWin=function(pos){
        var win = me.win, mask = me.mask;
        if(mask&&win) {
            m.Body.add(mask);
            m.Body.add(win);
            win.style.left = (mask.offsetWidth / 2 - win.offsetWidth / 2) + "px";
            if(pos=="bottom"){
                win.style.bottom="100px";
            }else{
                win.style.top = (mask.offsetHeight / 2 - win.offsetHeight / 2) + "px";
            }
            win.style.visibility = "visible";
            mask.style.visibility = "visible";
        }
    };
    me.show=function(p){
        //initial
        if(p) {sTitle = p.title||sTitle;fn= p.fn;sMsg= p.msg;}
        var win=createWinMsg(),mask=me.mask;
        renderWin();
        win.focus();
        return win;
    };
    me.wait=function(p,pos){
      var sTitle= p.msg||"加载中...";
      var win=createWinWait(),mask=me.mask;
        win.innerHTML=sTitle;
        renderWin(pos);
        win.focus();
        return win;
    }
})());
m.search=(new (function(){
    var me=this;
    var query=location.search;
    query=query.length>0?query.substring(1):"";
    var arrMix=query.split("&");
    for(var i=0;i<arrMix.length;i++){
        var item=arrMix[i].split("=");
        me[item[0]]=decodeURIComponent(item[1]);
    }
})());
m.onDomReady=function(f){
    //initialize the page
    var mtPg= m.dom.create("meta",{
        name:"viewport",
        content:"width=device-width,height=device-height,initial-scale=1,maximum-scale=1,user-scalable=no"
    });
    //add body style class
    m.Body.className="x-body";
    m.Head.appendChild(mtPg);
    /*Add key Events to form fields*/
    var arrInput=m.dom.queryAll(".x-form-item-input");
    var arrFText= m.dom.queryAll(".x-form-item-text");
    //Normal
    for(var i= 0;i<arrInput.length;i++){
        var elInput=arrInput.item(i);
        elInput.getValue=function(){return m.dom.getValue(this);};
        elInput.setValue=function(v){return m.dom.setValue(this,v)};
    }
    //Complex
    for(var h=0;h<arrFText.length;h++){
        var elText=arrFText[h];
        elText.getValue=function(){
            var elV=this.children[2];
            return elV.value;
        };
        elText.setValue=function(v){
            var elV=this.children[2];
            elV.value= v.toString();
        };
        elText.getText=function(){
            var elT=this.children[1];
            return elT.innerHTML;
        };
        elText.setText=function(v){
            var elT=this.children[1];
            elT.innerHTML=v;
        };
    }
    /*Add key method to template tag*/
    var arrTmp= m.dom.queryAll("TEMPLATE");
    for(var j=0;j<arrTmp.length;j++){var elTmp=arrTmp.item(j);elTmp.apply=function(data){return m.Temp.apply(this,data);}}
    /*Add expand menu behavior*/
    var arrEx= m.dom.queryAll(".x-menu-expand");
    for(var k=0;k<arrEx.length;k++){
       var item=arrEx[k],holder=item.children[0],body=item.lastElementChild;
       var iInX=0,iInY=0,iOX=0,iOY=0,iCX=0,iCY=0,bFocus=0,iLX=0,iLY= 0,dir/*direction*/;
        m.EventUtil.addListener(item,"touchstart",function(evt,b,c){
            if(!evt.touches.length) return;
            var touch=evt.touches[0];
            iInX= touch.pageX;
            iInY= touch.pageY;
            bFocus=true;
        },true);
        m.EventUtil.addListener(item,"touchmove",function(evt){
            evt.preventDefault();
            if(!evt.touches.length) return;
                var touch = evt.touches[0];
                iCX = touch.pageX;
                iCY = touch.pageY;
                iLX=iInX-iCX;
                iLY=iCY-iInY;
                if(iLX<-30) {
                        item.style.display = "none";
                }
                var deg=0;
                deg=Math.ceil(Math.atan2(iLY,iLX)*180/Math.PI+180);
        },true);
        m.EventUtil.addListener(item,"touchend",function(evt){
            if(!evt.touches.length) return;
            var touch=evt.touches[0];
            iOX= touch.pageX;
            iOY= touch.pageY;
            //分析方向
            bFocus=false;
        },true)
    }
    /*Add click method to tips*/
    var arrTip= m.dom.queryAll(".x-tip");
    for(var g=0;g<arrTip.length;g++){
        var tip=arrTip[g];
        m.addListener(tip,"click",function(){
           this.parentNode.removeChild(this);
        });
    }
    /*Fire {Event} Ready*/
    if(typeof f=="function"){f();}
};
m.init=function(){
    //Add global vars
    m.getDom= m.dom.get;m.removeDom= m.dom.remove;
    m.remove= m.dom.remove;m.get= m.dom.get;m.create= m.dom.create;
    m.Head= m.dom.head;m.Body= m.dom.body;m.Doc= m.dom.doc;m.Title= m.dom.title;
    m.dpr=window.devicePixelRatio||1;
    m.innerHeight=Math.ceil(window.innerHeight/ m.dpr);m.innerWidth=Math.ceil(window.innerWidth/ m.dpr);
    m.addListener=m.EventUtil.addListener;m.removeListener=m.EventUtil.removeListener;
    m.encode= m.JSON.encode;m.decode=m.JSON.decode;
    m.ajax= m.Ajax;
};
m.onReady=function(f){
    m.init();
    m.addListener(window,"DOMContentLoaded",function(){m.onDomReady(f);});
};